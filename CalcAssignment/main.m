//
//  main.m
//  CalcAssignment
//
//  Created by Justin on 2018-09-26.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
