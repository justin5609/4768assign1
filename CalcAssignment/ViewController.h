//
//  ViewController.h
//  CalcAssignment
//
//  Created by Justin on 2018-09-26.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic) BOOL userNumber; //check if user is typing a number
@property (nonatomic) float firstNumber; // First number entered by user
@property (nonatomic) float secondNumber; // Second number entered by user
@property (nonatomic, copy) NSString *operation; // plus, subtract, divide, etc. 

@end

