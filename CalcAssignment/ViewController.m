//
//  ViewController.m
//  CalcAssignment
//
//  Created by Justin on 2018-09-26.
//  Copyright © 2018 Justin. All rights reserved.
//

/*
    Justin Delaney
    201222684
    CS4768
    Assignment #1

    Local git repo was used as well as pushing to a remote repo on Gitlab, commits and history can be seen here:
     
 
    Known issues:
     - Operations button stretch to fit landscape mode but number buttons do not
     - Too much empty space between numbers/operations and =/DEL/C on larger iPhone screens in portrait mode
     - Need to press equals button after using C for calculator to function again
     
 
 */

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
- (IBAction)DecimalButton:(UIButton *)sender;                     // Various interfaces to handle each kind of button
- (IBAction)ClearButton:(UIButton *)sender;
- (IBAction)DeleteButton:(UIButton *)sender;
- (IBAction)equalsButton:(UIButton *)sender;
- (IBAction)OperationPressed:(UIButton *)sender;
- (IBAction)numberPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *CalcDisplay;

@end

@implementation ViewController

NSString *defaultScreen = @"0";

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor lightGrayColor]]; // Sets background  color of app
    self.CalcDisplay.layer.masksToBounds = true; // Rounds out corners of display screen; looks nicer
    self.CalcDisplay.layer.cornerRadius = 8;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)DecimalButton:(UIButton *)sender { // Handles user pressing the decimal button, Decimal does not currently change value of the float 
    if ([_CalcDisplay.text rangeOfString:@"."].location == NSNotFound) {
        _CalcDisplay.text = [_CalcDisplay.text stringByAppendingString:sender.titleLabel.text];
    }
    }

- (IBAction)ClearButton:(UIButton *)sender { // Pressing the CLEAR button resets the display to show 0 
    if (self.CalcDisplay.text != defaultScreen)
    {
        self.CalcDisplay.text = nil; // Need to press EQUALS after pressing this
    }
    
    
}

- (IBAction)DeleteButton:(UIButton *)sender { // Handles the user pressing the DEL button to backspace 
    if (_CalcDisplay.text != defaultScreen && _CalcDisplay.text.length > 1)
    {
        _CalcDisplay.text = [_CalcDisplay.text substringToIndex: [_CalcDisplay.text length] - 1];
    }
}

- (IBAction)equalsButton:(UIButton *)sender {     // This method performs the calculation depending on the operation button pressed
    self.userNumber = NO;
    self.secondNumber = [self.CalcDisplay.text floatValue];
    
    float result = 0.0; // Stores the result of the calculation
  
    if ([self.operation isEqualToString:@"+"])  // Statement for each possible operations button a user could press
    {
        result = self.firstNumber + self.secondNumber;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
    }
    
    else if([self.operation isEqualToString:@"-"])
    {
        result = self.firstNumber - self.secondNumber;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"x"])
    {
        result = self.firstNumber * self.secondNumber;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"÷"])
    {
        result = self.firstNumber / self.secondNumber;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"√"])
    {
        result = sqrt(self.firstNumber);
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"x^2"])
    {
        result = pow(self.firstNumber, 2);
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"x^3"])
    {
        result = pow(self.firstNumber, 3);
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"x^y"])
    {
        result = pow(self.firstNumber, self.secondNumber);
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
    else if ([self.operation isEqualToString:@"±"])
    {
        result = self.firstNumber * -1;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
    }
    
    else if ([self.operation isEqualToString:@"1/x"])
    {
        result = 1 / self.firstNumber;
        self.CalcDisplay.text = [NSString stringWithFormat:@"%0.2f", result];
        
    }
    
}

- (IBAction)OperationPressed:(UIButton *)sender {
    self.userNumber = NO;
    self.firstNumber = [self.CalcDisplay.text floatValue];
    self.operation = [sender currentTitle];
    
}

- (IBAction)numberPressed:(UIButton *)sender { // Displays the number what the user pressed
    NSString *number = sender.currentTitle;
    
    if(self.userNumber)
    {
        self.CalcDisplay.text = [self.CalcDisplay.text stringByAppendingString:number];
        
    }
    
    else {
        self.CalcDisplay.text = number;
        self.userNumber = YES;
    }
    
}
@end

