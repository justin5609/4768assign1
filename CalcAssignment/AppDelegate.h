//
//  AppDelegate.h
//  CalcAssignment
//
//  Created by Justin on 2018-09-26.
//  Copyright © 2018 Justin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

